package com.usahealth.computerinformationservice.dao;

import com.usahealth.computerinformationservice.entity.Ticket;
import com.usahealth.computerinformationservice.repository.TicketRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("ticketService")
public class TicketDAO {
    @Autowired
    private TicketRepository ticketRepository;

    public Ticket findById(Long id) throws NotFoundException{
        Optional ticket = ticketRepository.findById(id);
        if(ticket.isPresent()){
            return (Ticket) ticket.get();
        }
        else{
            throw new NotFoundException("Could not find computer report with ID "+id);
        }
    }

    public Ticket save(Ticket ticket){
        ticket = ticketRepository.save(ticket);
        return ticket;
    }

    public void delete(Ticket ticket){
        ticketRepository.delete(ticket);
    }

    public List<Ticket> list(){
        return ticketRepository.findAll();
    }
}
