package com.usahealth.computerinformationservice.dao;

import com.usahealth.computerinformationservice.entity.ComputerReport;
import com.usahealth.computerinformationservice.repository.ComputerReportRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("computerReportService")
public class ComputerReportDAO {
    @Autowired
    private ComputerReportRepository computerReportRepository;

    public ComputerReport findById(Long id) throws NotFoundException{
        Optional computerReport = computerReportRepository.findById(id);
        if(computerReport.isPresent()){
            return (ComputerReport) computerReport.get();
        }
        else{
            throw new NotFoundException("Could not find computer report with ID "+id);
        }
    }

    public List<ComputerReport> findByHostname(String hostname){
        List<ComputerReport> computerReports = computerReportRepository.findByHostName(hostname);
        computerReports.sort((computerReport1, computerReport2) -> -1*computerReport1.getCreatedAt().compareTo(computerReport2.getCreatedAt()));
        return computerReports;
    }

    public List<String> listHostNames(){
        List<String> hostNames = new ArrayList<>();
        List<ComputerReport> reports = computerReportRepository.findAll();
        reports.forEach(report->{
            if(!hostNames.contains(report.getHostName())){
                hostNames.add(report.getHostName());
            }
        });
        return hostNames;
        /*return computerReportRepository.findHostNames();*/
    }

    public ComputerReport save(ComputerReport computerReport){
        computerReport = computerReportRepository.save(computerReport);
        return computerReport;
    }

    public void delete(ComputerReport computerReport){
        computerReportRepository.delete(computerReport);
    }

    public List<ComputerReport> list(){
        return computerReportRepository.findAll();
    }
}
