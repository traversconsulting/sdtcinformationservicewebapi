package com.usahealth.computerinformationservice.dao;

import com.usahealth.computerinformationservice.entity.IpAddress;
import com.usahealth.computerinformationservice.repository.IPAddressRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("ipAddressService")
public class IPAddressDAO {
    @Autowired
    private IPAddressRepository ipAddressRepository;

    public IpAddress findById(Long id) throws NotFoundException{
        Optional ipAddress = ipAddressRepository.findById(id);
        if(ipAddress.isPresent()){
            return (IpAddress) ipAddress.get();
        }
        else{
            throw new NotFoundException("Could not find computer report with ID "+id);
        }
    }

    public IpAddress save(IpAddress ipAddress){
        ipAddress = ipAddressRepository.save(ipAddress);
        return ipAddress;
    }

    public void delete(IpAddress ipAddress){
        ipAddressRepository.delete(ipAddress);
    }

    public List<IpAddress> list(){
        return ipAddressRepository.findAll();
    }
}
