package com.usahealth.computerinformationservice.dao;

import com.usahealth.computerinformationservice.entity.Process;
import com.usahealth.computerinformationservice.repository.ProcessRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("processService")
public class ProcessDAO {
    @Autowired
    private ProcessRepository processRepository;

    public Process findById(Long id) throws NotFoundException{
        Optional process = processRepository.findById(id);
        if(process.isPresent()){
            return (Process) process.get();
        }
        else{
            throw new NotFoundException("Could not find computer report with ID "+id);
        }
    }

    public Process save(Process process){
        process = processRepository.save(process);
        return process;
    }

    public void delete(Process process){
        processRepository.delete(process);
    }

    public List<Process> list(){
        return processRepository.findAll();
    }
}
