package com.usahealth.computerinformationservice.dao;

import com.usahealth.computerinformationservice.entity.Drive;
import com.usahealth.computerinformationservice.entity.Drive;
import com.usahealth.computerinformationservice.repository.DriveRepository;
import com.usahealth.computerinformationservice.repository.DriveRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("driveService")
public class DriveDAO {
    @Autowired
    private DriveRepository driveRepository;

    public Drive findById(Long id) throws NotFoundException{
        Optional drive = driveRepository.findById(id);
        if(drive.isPresent()){
            return (Drive) drive.get();
        }
        else{
            throw new NotFoundException("Could not find computer report with ID "+id);
        }
    }

    public Drive save(Drive drive){
        drive = driveRepository.save(drive);
        return drive;
    }

    public void delete(Drive drive){
        driveRepository.delete(drive);
    }

    public List<Drive> list(){
        return driveRepository.findAll();
    }
}
