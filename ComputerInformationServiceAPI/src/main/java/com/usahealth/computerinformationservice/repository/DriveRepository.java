package com.usahealth.computerinformationservice.repository;

import com.usahealth.computerinformationservice.entity.Drive;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriveRepository extends JpaRepository<Drive, Long> {
}
