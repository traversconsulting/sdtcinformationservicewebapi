package com.usahealth.computerinformationservice.repository;

import com.usahealth.computerinformationservice.entity.IpAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPAddressRepository extends JpaRepository<IpAddress, Long> {
}
