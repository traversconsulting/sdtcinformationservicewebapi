package com.usahealth.computerinformationservice.repository;

import com.usahealth.computerinformationservice.entity.Process;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessRepository extends JpaRepository<Process, Long> {
}
