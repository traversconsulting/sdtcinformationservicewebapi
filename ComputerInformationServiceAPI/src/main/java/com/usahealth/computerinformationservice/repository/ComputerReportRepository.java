package com.usahealth.computerinformationservice.repository;

import com.usahealth.computerinformationservice.entity.ComputerReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ComputerReportRepository extends JpaRepository<ComputerReport, Long> {
    List<ComputerReport> findByHostName(String hostName);

    /*@Query("select ComputerReport.hostName from ComputerReport cr group by cr.hostName")
    List<String> findHostNames();*/
}
