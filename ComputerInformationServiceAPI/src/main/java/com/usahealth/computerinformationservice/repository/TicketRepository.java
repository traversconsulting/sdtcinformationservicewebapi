package com.usahealth.computerinformationservice.repository;

import com.usahealth.computerinformationservice.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
}
