package com.usahealth.computerinformationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.usahealth.computerinformationservice.dao","com.usahealth.computerinformationservice.config", "com.usahealth.computerinformationservice.controller", "com.usahealth.computerinformationservice.util", "com.usahealth.computerinformationservice.scheduled"})
@SpringBootApplication
public class ComputerInformationWebAPI {

	public static void main(String[] args) {
		SpringApplication.run(ComputerInformationWebAPI.class, args);
	}

}
