package com.usahealth.computerinformationservice.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service("jiraInterface")
public class JiraServiceDeskInterface {
    @Autowired
    private Environment env;

    public String submitTicket(String summary, String description) throws Exception{
        String json = "{\"serviceDeskId\":\"" + env.getProperty("jiraServiceDeskId") + "\", \"requestTypeId\":\"" + env.getProperty("requestTypeId")
                + "\", \"requestFieldValues\":{\"summary\": \""+summary+"\", \"description\": \""+description+"\"}}";
        //Necessary to send the new lines over json
        json = json.replace("\n", "\\n");
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(env.getProperty("jiraServiceDeskUrl") + "/rest/servicedeskapi/request");
        post.setHeader("User-Agent", "Mozilla/5.0");
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", "Basic " + env.getProperty("jiraAuthentication"));
        StringEntity params = new StringEntity(json);
        post.setEntity(params);
        HttpResponse response = client.execute(post);
        org.apache.http.HttpEntity entity = response.getEntity();
        String idResponse = EntityUtils.toString(entity);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(idResponse);
        if (jsonNode != null && jsonNode.get("issueKey") != null) {
            return "IssueKey: "+jsonNode.get("issueKey").textValue();
        } else {
            return idResponse;
        }
    }

}
