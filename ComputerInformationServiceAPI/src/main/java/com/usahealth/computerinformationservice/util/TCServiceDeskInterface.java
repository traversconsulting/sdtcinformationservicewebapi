package com.usahealth.computerinformationservice.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service("tcServiceDeskInterface")
public class TCServiceDeskInterface {
    @Autowired
    private Environment env;

    public String submitTicket(String desctiption) throws Exception{
        String json = String.format("{\"clientId\":%s, \"accountId\":%s, \"supportTechId\":%s, \"categoryId\":%s, \"statusId\":%s, \"actionMessage\":\"%s\"}",
                env.getProperty("tcClientId"), env.getProperty("tcAccountId"), env.getProperty("tcSupportTechId"), env.getProperty("tcCategoryId"), env.getProperty("tcStatusId"),
                desctiption);
        //HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(env.getProperty("tcServiceDeskUrl") + "/incidentapi/addincident");
        post.setHeader("User-Agent", "Mozilla/5.0");
        StringEntity params = new StringEntity(json);
        ArrayList<NameValuePair> postParameters;
        postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("parameters", json));
        post.addHeader("content-type", "application/x-www-form-urlencoded");
        post.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(post);
        HttpEntity entity = response.getEntity();
        String idResponse = EntityUtils.toString(entity);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(idResponse);
        if (jsonNode.get("id") != null) {
            return Long.toString(jsonNode.get("id").longValue());
        } else {
            return jsonNode.get("response").textValue();
        }
    }
}
