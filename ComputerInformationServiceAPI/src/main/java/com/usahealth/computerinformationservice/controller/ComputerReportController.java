package com.usahealth.computerinformationservice.controller;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usahealth.computerinformationservice.dao.*;
import com.usahealth.computerinformationservice.entity.ComputerReport;
import com.usahealth.computerinformationservice.entity.Drive;
import com.usahealth.computerinformationservice.entity.IpAddress;
import com.usahealth.computerinformationservice.entity.Process;
import com.usahealth.computerinformationservice.util.JiraServiceDeskInterface;
import com.usahealth.computerinformationservice.util.TCServiceDeskInterface;
import javassist.NotFoundException;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.logging.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ComputerReport")
@CommonsLog
public class ComputerReportController {
    @Autowired
    private ComputerReportDAO computerReportDAO;
    @Autowired
    private ProcessDAO processDAO;
    @Autowired
    private DriveDAO driveDAO;
    @Autowired
    private IPAddressDAO ipAddressDAO;
    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private TCServiceDeskInterface tcServiceDeskInterface;
    @Autowired
    private Environment env;

    @GetMapping("/{id}")
    public ComputerReport get(@PathVariable long id) throws NotFoundException {

        ComputerReport cr = (ComputerReport)  computerReportDAO.findById(id);
        if(cr == null){
            throw new NotFoundException("Computer Report with id "+id+" was not found.");
        }
        return cr;
    }

    @PostMapping("/")
    public ComputerReport save(@RequestParam Map<String, String> requestParams, @RequestBody ComputerReport m, BindingResult result, Model model) {
        if(m.getActiveProcess() != null) {
            m.setActiveProcess(processDAO.save(m.getActiveProcess()));
        }
        List<Drive> savedDrives = new ArrayList<>();
        final String hostName = m.getHostName();
        m.getDrives().forEach(drive-> {
            savedDrives.add(driveDAO.save(drive));
            try {
                double percentage = drive.getPercentageUsed();
                if (env.getProperty("autoSendHarddriveTickets").equalsIgnoreCase("true") && drive.isReady() && drive.getPercentageUsed() >= Double.valueOf(env.getProperty("harddriveCutOffPercentage"))) {
                    tcServiceDeskInterface.submitTicket("Harddrive " + drive.getDrive().replace("\\", "\\\\") + " on computer " + hostName + " drive has exceeded the allowed percentage " + env.getProperty("harddriveCutOffPercentage") + ". It is currently using " + drive.getPercentageUsed() + "%.");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });
        try {
            if (env.getProperty("autoSendProcessorTickets").equalsIgnoreCase("true") && m.getProcessorUsagePercent() >= Double.valueOf(env.getProperty("processorCutOffPercentage"))) {
                tcServiceDeskInterface.submitTicket("Computer " + m.getHostName() + " has exceed the allowed processor usage " + env.getProperty("processorCutOffPercentage") + ". It is currently using " + m.getProcessorUsagePercent() + "%.");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (env.getProperty("autoSendRamTickets").equalsIgnoreCase("true") && m.getRamUsage() >= Double.valueOf(env.getProperty("ramCutOffPercentage"))) {
                tcServiceDeskInterface.submitTicket("Computer " + m.getHostName() + " has exceed the allowed ram usage " + env.getProperty("ramCutOffPercentage") + ". It is currently using " + m.getRamUsage() + "%.");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        m.setDrives(savedDrives);
        List<IpAddress> savedIPAdresses = new ArrayList<>();
        m.getIpAddresses().forEach(ip-> {
            savedIPAdresses.add(ipAddressDAO.save(ip));
        });
        m.setIpAddresses(savedIPAdresses);
        if(m.getTicket() != null) {
            m.setTicket(ticketDAO.save(m.getTicket()));
            try {
                tcServiceDeskInterface.submitTicket(m.getTicket().getIssueType()+": "+m.getTicket().getDescription());
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        m = computerReportDAO.save(m);
        return m;
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable long id) throws NotFoundException {

        ComputerReport m = computerReportDAO.findById(id);
        if(m != null) {
            if(m.getActiveProcess() != null) {
                processDAO.delete(m.getActiveProcess());
            }
            m.getIpAddresses().forEach(ip->{
                ipAddressDAO.delete(ip);
            });
            m.getDrives().forEach(drive->{
                driveDAO.delete(drive);
            });
            if(m.getTicket() != null){
                ticketDAO.delete(m.getTicket());
            }
            computerReportDAO.delete(m);
            return "Computer Report with id "+id+" successfully deleted";

        } else {
            return "Computer Report with id "+id+" was not found and could not be deleted";
        }
    }


    @GetMapping("/list")
    public Map<String, List<ComputerReport>> list() {
        Map<String, List<ComputerReport>> outMap = new HashMap<String, List<ComputerReport>>();
        List<ComputerReport> list = computerReportDAO.list();
        outMap.put("data", list);
        return outMap;
    }
}
