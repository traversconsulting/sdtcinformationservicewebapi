/*
 * Travers Consulting
 *
 * [2014] - [2019] Travers Consulting Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Travers Consulting Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Travers Consulting Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Travers Consulting Incorporated.
 */

package com.usahealth.computerinformationservice.scheduled;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usahealth.computerinformationservice.dao.ComputerReportDAO;
import com.usahealth.computerinformationservice.entity.ComputerReport;
import com.usahealth.computerinformationservice.util.TCServiceDeskInterface;
import org.apache.http.HttpEntity;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@DisallowConcurrentExecution
public class JobCheckForInactiveComputers implements Job {
    @Autowired
    private ComputerReportDAO computerReportDAO;
    @Autowired
    private Environment env;
    @Autowired
    private TCServiceDeskInterface tcServiceDeskInterface;

    public void execute(JobExecutionContext context) throws JobExecutionException {
        if(env.getProperty("autoSendNoCheckInTickets").equalsIgnoreCase("true")) {
            List<String> hostNames = computerReportDAO.listHostNames();
            hostNames.forEach(hostName -> {
                List<ComputerReport> computerReports = computerReportDAO.findByHostname(hostName);
                if (computerReports.size() != 0) {
                    ComputerReport mostRecentCR = computerReports.get(0);
                    final int MILLI_TO_HOUR = 1000 * 60 * 60;
                    long hoursBetween = (new Date().getTime() - mostRecentCR.getCreatedAt().getTime()) / MILLI_TO_HOUR;
                    if (Long.valueOf(env.getProperty("timeInHoursSinceLastCheckInCutOff")) <= hoursBetween) {
                        try {
                            tcServiceDeskInterface.submitTicket("Computer with hostname: " + hostName + " has not checked in in " + hoursBetween + " hours.");
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        }


    }
}

