package com.usahealth.computerinformationservice.entity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "Drive")
public class Drive {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    private String drive;
    @Column
    private String type;
    @Column
    private String filesystemType;
    @Column
    @JsonProperty
    private boolean isReady;
    @Column
    private long userFreeSpace;
    @Column
    private long totalFreeSpace;
    @Column
    private long totalSize;

    public double getPercentageUsed(){
        double usedSpace = this.totalSize-this.totalFreeSpace;
        double usedPercentage = (usedSpace/(double)totalSize)*100;
        return usedPercentage;
    }
}