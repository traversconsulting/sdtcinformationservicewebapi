package com.usahealth.computerinformationservice.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    private String issueType;

    @Column
    private String description;
}
