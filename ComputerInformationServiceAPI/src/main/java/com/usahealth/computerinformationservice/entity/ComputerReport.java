package com.usahealth.computerinformationservice.entity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "ComputerReport")
public class ComputerReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    private Date createdAt;
    @Column
    private double processorUsagePercent;

    @Column
    private String userName;
    @OneToOne
    private Process activeProcess;
    @OneToMany
    private List<Drive> drives;
    @Column
    private String hostName;
    @OneToMany
    private List<IpAddress> ipAddresses;
    @Column
    private double ramUsage;

    @OneToOne
    private Ticket ticket;


}

/*
{"userName":"STravers",
"createdAt":"2020-04-10"
"drives":
    [{"drive":"C:\\",
    "type":"Fixed",
    "filesystemType": "NTFS",
    "isReady":"True",
    "userFreeSpace" : "27902451712",
    "totalFreeSpace" : "27902451712",
    "totalSize" : "126326140928"}],
"hostName" : "DR001",
"ipAddresses":[{"ipAddress":"192.168.155.11"},{"ipAddress":"192.168.129.1"}],
"ramUsage":"90.11% RAM Used"}
 */