package com.usahealth.computerinformationservice.entity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Process")
public class Process {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    private String title;
    @Column(columnDefinition = "TEXT")
    private String versionInfo;
 }