package com.usahealth.computerinformationservice.entity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "IpAddress")
public class IpAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    private String ipAddress;
}